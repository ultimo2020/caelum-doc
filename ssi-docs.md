# Epics

## 1 - Identity Smart Contracts

We need tow smart contracts. One to be able to manage the Main Smart Contract for Identity, it will allow to implement recovery strategies, changes management... The other contract is the Identity Contract.

Developed with Truffle + Unit Tests.

## 2 - Create Entity - Organization

1. No Parity running? Install parity first - get ID of the network
2. Ask for a password
3. User, organization or IoT? Organization :)
4. Create a new wallet for this entity with the password (Management Wallet).
5. Store it at ~/.ssi/entity.utc
6. Deploy the Management Contract. Get the address MA
7. Deploy the Identity Contract
8. Create an empty DIDs Document and save it on IPFS.
9. Update the Identity Contract with the DID.
10. Store everything on MongoDB

## 3 - View Data        

## 3 - Add a Key



## 4 - Add a self Claim

With the new wallet deploys a new Identity Smart Contract
4. Gets the DID as a result (deployed Smart Contract)
5. New pair of Keys (Login Wallet)
6. Creates a DID Document : DID + public Key


TODO: Add claims. name, phone...

## Add Organization

1. Organization Password
2. Create a new V3 wallet for this user with the password (Management Wallet)
3. With the new wallet deploys a new Identity Smart Contract
4. Gets the DID as a result (deployed Smart Contract)
5. Creates a DID Document : DID + admin public Key as owner
6. Assign an alias

TODO: Add claims (signed by admin). Name phone....

## Start APP

Execute rust-ssi

Options : + (adds new user), l (list users), d <alias>

## Add extra users

Dani
em cal un servidor que tingui diversos dockers interns segurs (encriptats). Aquest docker s'ha de poder comunicar amb el server on hi ha el Blockchain. Cada docker respondrà a un port concret que estarà enrutat per una url (user1.caelumlabs.com => 3000, user2.caelumlabs.com => 3001...). Ha de ser fàcil fer una copia de seguretat del docker per aixecar-lo tal quan en un altre lloc.


Milestones

The objective is to do a Login and to exchange a claim between two companies.

1. Rust server : one identity as organization (Alastria)
2. Rust server : one identity (Caelum), get Alastria Claim (verified by Alastria)
3. Rust server : one identity as persona (Alex Puig)
4. React Native : Load keys from terminal for Alex (show QR in terminal)
5. Login to Caelum Labs from Mobile App to Rust server (Caelum). Get a JWT.
6. Call test function in Rust Server (Caelum) using the JWT.
7. Task list via API. Alex to Caelum

4. Rust client : login to Alastria as Alex Puig

4. Rust server + client : two identities (Caelum + Alex)
5. Rust server + client : add contacts
